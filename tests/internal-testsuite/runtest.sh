#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tools/libmpc/Regression/internal-testsuite
#   Description: internal-testsuite
#   Author: Martin Coufal <mcoufal@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2016 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="${PACKAGE:-$(rpm -qf --qf='%{name}\n' /usr/lib64/libmpc.so.3)}"
export PACKAGE

rlJournalStart
    rlPhaseStartSetup
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
        rlRun "pushd $TmpDir"
	rlFetchSrcForInstalled $PACKAGE
	rlAssertRpm $PACKAGE
	rlRun "dnf builddep -y $PACKAGE"
        rlRun "rpm --define='_topdir $TmpDir' -Uvh *src.rpm"
        rlRun "rpmbuild --define='_topdir $TmpDir' -bc SPECS/libmpc.spec"
    rlPhaseEnd

    rlPhaseStartTest
	rlRun "pushd BUILD/mpc-*"
        # test
        rlRun "make RUNTESTFLAGS='--tool_exec=$BIN' check |& tee $TmpDir/check.log"
        rlRun "popd"
        # no unexpected results should appear
        rlRun "grep '^# of unexpected' check.log" 1
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
